# Change log

## 1.0.0-TODO
The distant future; these features are out of scope for v0.x:
* Discover APIs (and warn that this should be configured?  cache?), generate matching client.
* Wikimedia Commons media commands.
* Clients for some of the many other Wikimedia [REST
API](https://www.mediawiki.org/wiki/REST_API )s beyond core, served through
RESTBase. See [issue #2](https://gitlab.com/adamwight/mediawiki_client_ex/-/issues/2).
* Demonstrate a cross-wiki API call (CentralAuth).
* Dump-processing interface for the archives served on
https://dumps.wikimedia.org/ — might be blocked by the unavailability of bzip2
and 7zip bindings for Erlang or Elixir.
* meter how many calls are made, when, and bandwidth used.
* Wikidata Query Service

## 0.99.0-TODO
Holding place for everything needed to finish the 0.x series.  The focus is on a few robust, core apis.

What it should already include:
* Detect server and network errors, fail fast.  Show helpful API debugging in dev environment.  Demonstrate how to call with error handling.
* Longer, configurable default timeouts to match servers. (#18)
* Convenient logging—a global setting to inject the logging middleware into all client plugs.
* ...

## 0.5.0-TODO
This is the development target and next steps.
* Show how to integrate with the MediaWiki OAuth2 provider (to be published), to authenticate actions on behalf of a user.
* Built-in Mediawiki [REST API](https://www.mediawiki.org/wiki/API:REST_API)
* ...

## 0.4.2-TODO
* ...

## 0.4.1 (Dec 2022)
* Now compatible with tesla ~>1.5
* Fix compression for Action and ORES APIs

## 0.4.0 (Sep 2022)
Breaking changes:
* Config variables are deprecated, all customization is done via keyword options.
* Wiki.EventStreams `:send_to` option renamed to `:stream_to`.

Other:
* `mix test.all` task replaces git_hooks for development.
* ...

## 0.3.1 (Sep 2022)
Breaking changes:
* Wiki.Site renamed to Wiki.SiteMatrix and has an updated interface.  `new()` returns an opaque sitematrix.
* Settle on the Apache 2 license (bug #21).

Other:
* Wiki.SiteMatrix caches the response.

## 0.3.0 (Dec 2021)
Breaking changes:
* Switched most of the API to return `{:ok | :error, ...}` tuples.  For quick migration just use the bang functions like `get!`.  Or match `{:ok, result}` for fine-grained error handling.  Errors will be returned as exception objects, with a crude string message for now.  In a future release these will include a reason atom.

## 0.2.6 (Dec 2021)
Breaking changes:
* Renamed the package from `wiki_elixir` to `mediawiki_client`.
`mix.exs` dependencies must be updated.
* OTP app name in configuration must also be changed from `wiki_elixir` to `mediawiki_client`.
* The package namespace is still `Wiki.`-, because I honor your labors, noble reader.

## 0.2.5 (Nov 2021)
New features:
* `Wiki.Site` module for retrieving the Wikimedia site matrix.
* You may migrate `Wiki.Action.new()` calls to accept a `Wiki.Site` object,
which will be used to calculate the default endpoint.

## 0.2.2 (Nov 2021)
* Depend on eventsource_ex library, testability improvements have been upstreamed.
* Continuous tests verify compatibility with Elixir 1.9-1.12 .
* User-agent header sends correct library version.
* Drop unused dependencies.

## 0.2.1 (May 2021)

Breaking changes:
* Request query parameters are passed as a keyword list rather than a map.
* Handles application-level API errors in the action and ores apis, failing fast.

Other:
* Complete test coverage for both happy and sad cases.
* Remove unused `Timex` dependency.
* Allow literal "|" in parameters by switching the delimiter to "unit separator".
* Default to action response format version 2.

## 0.1.4 (May 2020)

* Fix application configuration bug, nothing worked out of the box.
* Fix a continuation bug which would drop the first response.
* Removed the incomplete `Wiki.Rest` client.
* Some test coverage for `Wiki.Action`.
* Add lint jobs to git hook and GitLab CI.

## 0.1.2 (May 2020)

* Rename WikiAction -> `Wiki.Action`
* Rename WikiRest -> `Wiki.Rest`
* Rename WikiSSE -> `Wiki.EventStreams`
* Basic ORES client.
* Inline examples as module documentation.
* Pipe-join list values for Action API.
* Accumulate Action results.

## 0.1.1 (May 2020)

* Send User-Agent header.
* Action API and continuations.  Authentication, session cookies.
* Pipelining.
* Flexible endpoint.
* Server-side events relayed as a `Stream`.
* Simplify and package examples as scripts.
* Begin work on REST API.
* Host code on GitLab, apply the most basic CI.
* Temporarily inline the [cwc/eventsource_ex](https://github.com/cwc/eventsource_ex/)
server-side events library as a workaround.
* Switch to Tesla HTTP client.

## 0.1.0 (May 2019)

* Initial release.
